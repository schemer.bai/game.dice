import * as _ from 'lodash'
import * as assert from 'assert'
import {dice, parse} from '../index'

describe('test dice', () => {

  it("test parse", () => {

    let d = parse('2d6 + 1', true)
    assert.deepEqual(d, { times: 2, face: 6, modifier: 1, isFloat: true })

    d = parse('3d7.5 + 3 - 1', false)
    assert.deepEqual(d, { times: 3, face: 7.5, modifier: 2, isFloat: false })

  })

  it("test set Random", () => {

    let d = dice().of(1, 6, 1).setRandom(() => 0.01)
    let set = new Set()
    _.times(10, () => set.add(d.roll()))
    assert.equal(set.size, 1)
    _.times(10, () => set.add(d.roll(() => 0.99)))
    assert.equal(set.size, 2)
  })

  it("test float", () => {

    let d = dice('2d6.5', true)
    assert.equal(d["dice"].times, 2)
    assert.equal(d["dice"].face, 6.5)
    let map = new Map<number, number>()

    _.times(1000, () => {
      let t = Math.round(d.roll())
      let n = map.get(t)
      if (n) n ++
      else n = 1
      map.set(t, n)
    })

    let arr = _.entries(map).sort((a, b) => (a[0] as any) - (b[0] as any))
    console.log(arr)
  })

  it("test integer", () => {

    let d = dice('2d6')
    assert.equal(d["dice"].times, 2)
    assert.equal(d["dice"].face, 6)
    let map = new Map<number, number>()

    _.times(100, () => {
      let t = d.roll()
      let n = map.get(t)
      if (n) n ++
      else n = 1
      map.set(t, n)
    })

    let arr = _.entries(map).sort((a, b) => (a[0] as any) - (b[0] as any))
    console.log(arr)
  })

})