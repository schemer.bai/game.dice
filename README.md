# d6.5
this is a package to generate D&amp;D dice

## Interface
``` ts
export interface IDice {
    times: number;
    face: number;
    modifier: number;
    isFloat: boolean;
}
export declare class DiceWrapper {
    private dice;
    private random;
    constructor(dice?: IDice);
    of(times: number, face: number, modifier?: number, isFloat?: boolean): this;
    setModifier(modifier: number): this;
    setRandom(random: () => number): this;
    roll(random?: () => number): number;
}
/**
 * use a input to generate a dice,
 * format: 'dm' 'ndm' 'ndm [+-]k'
 * @param {string} input string that generate the dice
 */
export declare function dice(input?: string, isFloat?: boolean): DiceWrapper;
export declare function rollDice(input: string, isFloat?: boolean): number;
export declare function parse(input: string, isFloat: boolean): IDice;

```
