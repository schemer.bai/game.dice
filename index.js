"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DiceError extends Error {
}
var defaultRandom = Math.random;
class DiceWrapper {
    constructor(dice) {
        this.random = defaultRandom;
        this.dice = dice || {
            times: 1, face: 6, modifier: 0, isFloat: false
        };
    }
    of(times, face, modifier = 0, isFloat = false) {
        this.dice.times = times;
        this.dice.face = face;
        this.dice.modifier = modifier;
        this.dice.isFloat = isFloat;
        return this;
    }
    setModifier(modifier) {
        this.dice.modifier = modifier;
        return this;
    }
    setRandom(random) {
        this.random = random;
        return this;
    }
    roll(random = this.random) {
        let { times, face, modifier, isFloat } = this.dice;
        if (times < 0)
            throw new DiceError('roll dice, times must >=0');
        if (face <= 0)
            throw new DiceError('roll dice, face must >0');
        if (isFloat) {
            let sumrnd = 0;
            for (let i = times; i >= 1; i--) {
                sumrnd += random();
            }
            return sumrnd * face + modifier;
        }
        else {
            let sumrnd = 0;
            for (let i = times; i >= 1; i--) {
                sumrnd += ((random() * face) | 0) + 1;
            }
            return sumrnd + modifier;
        }
    }
}
exports.DiceWrapper = DiceWrapper;
/**
 * use a input to generate a dice,
 * format: 'dm' 'ndm' 'ndm [+-]k'
 * @param {string} input string that generate the dice
 */
function dice(input, isFloat) {
    if (input) {
        let dice = parse(input, isFloat || false);
        return new DiceWrapper(dice);
    }
    else {
        return new DiceWrapper();
    }
}
exports.dice = dice;
function rollDice(input, isFloat = false) {
    return dice(input, isFloat).roll();
}
exports.rollDice = rollDice;
function parse(input, isFloat) {
    let result = { times: 1, face: 6, modifier: 0, isFloat };
    let match = input.match(/^\s*([\d\.]+)?\s*[dD]\s*([\d\.]+)\s*(.*)\s*$/);
    if (match) {
        if (match[1]) {
            result.times = parseFloat(match[1]);
        }
        if (match[2]) {
            result.face = parseFloat(match[2]);
        }
        if (match[3]) {
            let modifiers = match[3].match(/([+-]\s*\d+)/g);
            if (modifiers)
                for (let i = 0; i < modifiers.length; i++) {
                    result.modifier += parseInt(modifiers[i].replace(/\s/g, ''));
                }
            else
                throw new DiceError(`dice parse match3 error: ${match[3]} - ${input}`);
        }
    }
    else {
        throw new DiceError('dice parse error {expect `xdn + m`}: ' + input);
    }
    return result;
}
exports.parse = parse;
